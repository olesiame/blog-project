import React, { Component } from "react"

import "./common/style/reset.css"
import "./common/style/style.css"

import Header from "./App/Header/Header"
import Main from "./App/Main/Main"
import Footer from "./App/Footer/Footer"

class App extends Component {

  render () {
    return (
      <div>
        <Header/>
        <Main/>
        <Footer/>
      </div>
        )
    }
  }

export default App