import React from "react"

const Categories = () => (

    <div>
        <div class="categories"><h3>Categories:</h3></div>
        <button class="filter-btn-active first-btn" onclick="selectAll">Show all</button>
        <button class="filter-btn second-btn" onclick="selectPolitics">Politics</button>
        <button class="filter-btn third-btn" onclick="selectEconomy">Economy</button>
        <button class="filter-btn forth-btn" onclick="selectEntertaiment">Entertaiment</button>
    </div>
)

export default Categories