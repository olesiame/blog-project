import React, { Component } from "react"
import PropTypes from 'prop-types'

import {Link} from "react-router-dom"

class NewsPropsDeclared extends Component {

    render() {

        const {
            id,
            image,
            heading,
            date,
            subHeading,
            description,
        } = this.props

        return (
            <div className="news">
                    <div className="news-facebook"><Link to="./"></Link></div>
                    <div className="news-twitter"><Link to="./"></Link></div>
                <div className="news-image">
                    <Link to={`/news-page/${id}`}>
                        <img src={image} alt="" width="605px"/>
                    </Link>
                </div>
                <div className="news-text">
                    <Link to={`/news-page/${id}`}>
                        <h1>{heading}</h1>
                    </Link>
                    <h6>{date}</h6>
                    <h4>{subHeading}</h4>
                    <p>{description}</p>
                </div>
            </div>
        )
    }
}

NewsPropsDeclared.propTypes = {
    image: PropTypes.string,
    heading: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    subHeading: PropTypes.number.isRequired,
    description: PropTypes.string,
    category: PropTypes.bool,
}

NewsPropsDeclared.defaultProps = {
    description: "No description ..."
}

export default NewsPropsDeclared