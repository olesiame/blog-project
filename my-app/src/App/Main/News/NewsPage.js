import React from 'react'
import newsProps,{getNewsArticle} from './NewsProps'

const NewsPage = ({
    id,
    match,
    image,
    heading,
    date,
    subHeading,
    description,
    articleInfo = getNewsArticle(newsProps)
}) => {
    return (
        <div className="news">
            <div className="news-facebook"></div>
                <div className="news-twitter"></div>
                <div className="news-image">
                    <img src={articleInfo[match.params.id].image} alt="" width="605px"/>
                </div>
                <div className="news-text">
                <h1>{articleInfo[match.params.id].heading}</h1>
                <h6>{articleInfo[match.params.id].date}</h6>
                <h4>{articleInfo[match.params.id].subHeading}</h4>
                <p>{articleInfo[match.params.id].description}</p>
            </div>
        </div>
  ) 
}

export default NewsPage