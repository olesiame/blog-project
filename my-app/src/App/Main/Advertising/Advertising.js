import React from "react"

import ad from "./ad.png"
    
const Advertising = () => {
    return (
    <div className="advertising">
        <img src={ad} alt="layout ad" name="layout ad"/>
    </div>
    )
}

export default Advertising