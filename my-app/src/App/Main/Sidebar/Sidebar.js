import React from "react"    
    
const Sidebar = () => {
    return (
<div className="sidebar">
    <div className="design-blog">
        <h3>Creative+ Design Blog</h3>
        <h6>Some text here</h6>
        <p><span className="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, ea, dignissimos. Deleniti debitis soluta tenetur accusamus asperiores minus nulla consectetur sequi quam, dicta? Possimus beatae aperiam voluptatum voluptas molestias minus.</span></p>
    </div>
        <div className="updates enter">
            <form method="post">
                <h3>Receive updates</h3>
                <div className="subscribe">
                    <input type="email" placeholder="Leave your email here" name="email"/>
                    <input type= "submit" value="Join"/>
                </div>
                    <p>
                        <span className="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, est.</span>
                    </p>
            </form>
                </div>
                    <div className="recent-posts enter">
                        <h3>Recent posts</h3>
                            <ul>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    <h6>July 7, 2019</h6></li>
                                <li>Maiores vero provident laboriosam est, ea rem sapiente explicabo earum itaque nihil, nostrum saepe nesciunt vitae.
                                    <h6>July 8, 2019</h6></li>
                                <li>Quia ut tempora ullam voluptate ducimus.
                                    <h6>July 9, 2019</h6></li>
                            </ul>
                        </div>
                    <div className="popular-tags enter">
                        <h3>Popular tags</h3>
                        <button>Cats</button>
                        <button>Dogs</button>
                        <button>Cat</button>
                        <button>Dog</button>
                        <button>Cat here</button>
                    </div>
                 </div>     
    )
}

export default Sidebar