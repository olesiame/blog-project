import React from "react"
import {Route, Switch} from 'react-router-dom'

import Socials from "./Socials/Socials"
import News from "./News/News"
import Sidebar from "./Sidebar/Sidebar"
import Advertising from "./Advertising/Advertising"

import NewsPage from "./News/NewsPage"

import Articles from "./Articles/Articles"
import Tutorials from "./Tutorials/Tutorials"
import Design from "./Design/Design"
import WebDesign from "./Web design/WebDesign"
import Other from "./Other/Other"
import Contacts from "./Contacts/Contacts.js"

const Main = () => {
    return (
        <main>
            <Switch>
                <div className="container">
                    <div className="row">
                        <div className="col-left">
                            <Socials/>
                            <div className="col-left-main">
                            <Route exact path="/" component={News}/>
                            <Route path="/news-page/:id" component={NewsPage}/>
                            <Route path="/articles" component={Articles}/>
                            <Route path="/tutorials" component={Tutorials}/>
                            <Route path="/design" component={Design}/>
                            <Route path="/tutorials" component={Tutorials}/>
                            <Route path="/web-design" component={WebDesign}/>
                            <Route path="/other" component={Other}/>
                            <Route path="/contacts" component={Contacts}/>
                            </div>
                        </div>
                        <div className="col-right">
                            <Sidebar/>
                            <Advertising/>
                            </div>
                        </div>	
                    </div>
                </Switch>
            </main>
        )
    }

export default Main