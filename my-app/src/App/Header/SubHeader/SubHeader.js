import React from "react"

import subheader from "./subHeader.png"

const SubHeader = () => {
    return (
        <div class="subHeader">
            <img src={subheader} alt="" width="100%"/>
        </div>
    )
}

export default SubHeader