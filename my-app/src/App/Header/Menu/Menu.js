import React from "react"

import {Link} from 'react-router-dom'

const Menu = () => {
    return (
        <div class="menu">
        <nav>
            <ul>
                <li><Link to="/articles">Articles</Link></li>
                <li><Link to="/tutorials">Tutorials</Link></li>
                <li><Link to="/design">Design</Link></li>
                <li><Link to="/web-design">Web design</Link></li>
                <li><Link to="/other">Other</Link></li>
                <li><Link to="/contacts">Contacts</Link></li>
            </ul>
        </nav>
    </div>
    )
}

export default Menu