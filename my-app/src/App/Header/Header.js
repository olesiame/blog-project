import React, {Component} from "react"

import Logo from "./Logo/Logo"
import Menu from "./Menu/Menu"
import SubHeader from "./SubHeader/SubHeader"

class Header extends Component {
    render () {
    return (
        <header>
            <div class="header">
                <Logo/>
                <Menu/>
            </div>
                <SubHeader/>
        </header>
        )
    }
}

export default Header