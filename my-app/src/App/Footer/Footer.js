import React, {Component} from "react"

import Copyright from "./Copyright/Copyright"

class Footer extends Component {
    render () {
    return (
            <footer className="footer">
                <Copyright/>
            </footer> 
        )
    }
}

export default Footer