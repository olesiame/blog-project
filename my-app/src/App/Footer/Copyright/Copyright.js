import React from "react"

const Copyright = () => {
    return (
        <div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias iure facere dolorum adipisci eum? Saepe, itaque.</p>
            <div className="footer-facebook"><a href=""></a></div>
            <div className="footer-twitter"><a href=""></a></div>
            <div className="footer-instagram"><a href=""></a></div>
                <form method="post">
                    <input type="email" placeholder="Join our newsletter"/>
                    <input type="submit" value="Join"/>
                </form>
                    <div className="copyright">Copyright 2019</div>
        </div>
    )
}

export default Copyright